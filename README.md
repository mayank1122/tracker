# **Issue Tracker** #
A Nodejs issue tracker application

## **Getting Started** ##

## **Prerequisites** ##
* Node.js
* MongoDB

## **Developing** ##
* Run npm start to start server.

## **Screenshots** ##
![step 1.png](https://bitbucket.org/repo/kLajXy/images/1877247438-step%201.png)![step 2.png](https://bitbucket.org/repo/kLajXy/images/1680785584-step%202.png)![step 3.png](https://bitbucket.org/repo/kLajXy/images/3465995556-step%203.png)![step 4.png](https://bitbucket.org/repo/kLajXy/images/4115991990-step%204.png)![step 5.png](https://bitbucket.org/repo/kLajXy/images/2065097258-step%205.png)![step 6.png](https://bitbucket.org/repo/kLajXy/images/1999958817-step%206.png)![step 7.png](https://bitbucket.org/repo/kLajXy/images/1206903045-step%207.png)![step 7.1.png](https://bitbucket.org/repo/kLajXy/images/2324132147-step%207.1.png)![step 8.png](https://bitbucket.org/repo/kLajXy/images/3375176262-step%208.png)![step 9.png](https://bitbucket.org/repo/kLajXy/images/3668031160-step%209.png)